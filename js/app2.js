(function() {
// modelの生成
var Task = Backbone.Model.extend({
  defaults: {
    title: "backbone_backbone",
    completed: false
  }
});
// インスタンス化
var task = new Task();

// Viewの生成
var TaskView = Backbone.View.extend({
  tagName: 'li',
  // className: 'liClass',
  // id: 'liId'

  // テンプレートを使ってmodelの中身で作る
  tmpl: _.template("title => <%- title %>"),
  render: function() {
    console.log(this.model.toJSON());
    var html = this.tmpl( this.model.toJSON() );
    console.log(html);
    this.$el.html(html)
  }

});

// View作成の際にmodelを渡す
// liClassに名前をつける
var taskView = new TaskView({ model: task });
taskView.render();

// 中身を確認(変数名.elとなっている点に注意)
// $elとするとjQueryのオブジェクトになり、メソッドが使えるようになる。
console.log(taskView.$el);

})();
