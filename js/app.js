(function() {

// model生成
var Task = Backbone.Model.extend({
  defaults: {
    title: 'do something',
    completed: false
  },
  // validationをかける
  validate: function(attr) {
    if ( _.isEmpty(attr.title) ) {
      return 'TODOのタイトルが入力されていません!!!';
    }
  },
  // 表示させる部分、modelにinitializeを書く
  initialize: function() {
    // validationがうまくいかなかった時の処理(invalid)
    // ここのfunction()は、modelとerrorという引数を取る
    // model=大元のmodel, error
    this.on('invalid', function(model, error) {
      $('#error').html(error);
    })
  }
});

// collection生成
var Tasks = Backbone.Collection.extend({ model: Task });

// view生成
var TaskView = Backbone.View.extend({
  tagName: 'li',
  // TaskViewからmodelをウォッチ?する部分が必要 (決まり文句?)
  initialize: function(){
    // modelに変更があった時の監視をここでやっている

    // 変更があった際、描画の変更をやり直す
    this.model.on('change', this.render, this); //<- 最後のthisはコンテクスト
    // 変更があった際、modelを削除
    this.model.on('destroy', this.remove, this); //<- 最後のthisはコンテクスト
  },
  events: {
    // トグルボタンイベント
    'click .toggle': 'toggle',
    // 削除ボタンクリックイベント
    'click .delete': 'destroy'
  },
  toggle: function() {
    //completedの値を!反転させる  ↓
    this.model.set('completed', !this.model.get('completed'));
  },
  destroy: function() {
    // 確認のポップアップ
    if (confirm('本当に削除しますか？')){
      // 見た目と同時にmodelも破壊する
      this.model.destroy();
    }
  },
  remove: function() {
    this.$el.remove();
  },
  template: _.template($('#task-template').html()),
  render: function() {
    var template = this.template(this.model.toJSON());
    this.$el.html(template);
    return this;
  }
});

// 複数処理させるview生成
var TasksView = Backbone.View.extend({
  tagName: 'ul',
  // collectionに変更があったら画面に追加する
  initialize: function(){
    this.collection.on('add', this.addNew, this);
    // 残りタスク数を追加、チェック、削除時など動的にカウントさせる
    this.collection.on('change', this.updateCount, this);
    this.collection.on('destroy', this.updateCount, this);
  },
  addNew: function(task) {
    var taskView = new TaskView({model: task});
    this.$el.append(taskView.render().el);
    // 項目追加後入力フォームをクリアする+フォーカスをかける
    $('#title').val('').focus();
    // addNew時にもカウントする必要があるのでここにも記載
    this.updateCount();
  },
  // 返ってくるthis.collectionをカウントする部分作成
  updateCount: function() {
    // filterはusのメソッド
    var uncompletedTasks = this.collection.filter(function(task) {
      return !task.get('completed');
    });
    // 上で取ってきたuncompletedTasksの数をcountに入れればOK
    $('#count').html(uncompletedTasks.length);
  },
  render: function() {
    this.collection.each(function(task) {
      var taskView = new TaskView({model: task});
      this.$el.append(taskView.render().el);
    }, this); // contextをthisに

    // updateCountをrenderする時にここでメソッドを呼ぶ
    this.updateCount();

    return this; // htmlに渡す際 .html(tasksView.render().el と後で書くために
  }              // ここでreturn this; して処理内容を返す
});

// formに対してのロジック追加
var AddTaskView = Backbone.View.extend({
  // 元からある要素を使う為、tagNameではなくいきなりelから書く
  el: '#addTask',
  // events設定
  events: {
    'submit': 'submit'
  },
  // function()定義して実行
  submit: function(e) {
    e.preventDefault();

    // var task = new Task({title: $('#title').val()});

    // validationは値をセットする時にしか動かないので
    // 単純にaddで追加される処理を書いていたこの部分に少し手を加える。

    var task = new Task();
    if (task.set({title: $('#title').val()}, {validate: true})) {
      this.collection.add(task);
      // 出ぱなしでのエラー文をsubmit後,値を空にする処理を書き
      // 正常に処理されたら消えるようにする
      $('#error').empty();
    }

  }
});

var tasks = new Tasks([
  {
    title: 'やること1',
    completed: true
  },
  {
    title: 'やること2'
  },
  {
    title: 'やること3'
  }
]);

// ロジック(View)のインスタンス化
var tasksView2 = new TasksView({collection: tasks});
var addTaskView = new AddTaskView({collection: tasks});

$('#tasks').html(tasksView2.render().el);

})();
