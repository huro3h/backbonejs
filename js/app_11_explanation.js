(function() {
// modelの生成
var Task = Backbone.Model.extend({
  defaults: {
    title: "backbone_backbone",
    completed: false
  }
});
// インスタンス化
var task = new Task();

// Viewの生成
var TaskView = Backbone.View.extend({
  tagName: 'li',

  // templateの中身を外部に投げる
  // idを指定した後に.htmlで htmlを取ってくる
  tmpl: _.template( $('#task-t').html() ),
  render: function() {
    var html = this.tmpl( this.model.toJSON() );
    this.$el.html(html);
    return this;
  }
});

//Collection

var Tasks = Backbone.Collection.extend({
  model: Task
});

// Viewの生成
var TasksView = Backbone.View.extend({
  tagName: 'ul',　// collection内1つ1つのタグがliなので、それらをまとめるulタグをここで使用
  // renderをかけて描画していく
  render: function() {
    // 実際インスタンス化する際は、以下のtasksをcollection経由でもらうのでthis.collectionとなる
    // .eachで1つ1つに処理していく　　　↓のtask(変数)を使ってtask viewを作る
    this.collection.each(function(task) {
       var taskView = new TaskView({model: task});
       // 出てきたli要素を.appendでくっつけていってる
       this.$el.append(taskView.render().el);
       //  ちょっと難しいcontextのthis、これがないと先に書いたfunctionのcontextが
       //  何を指しているのかわからなくなり、グローバルなものを指しにいく結果になる。
       // ↓引数にthisを与えることで、外側のthisは「内側でul要素を指している」thisと見なされる。
    }, this);
    // returnでthisを返してやることでtasksView.render().elと書けるようになる
    // returnしなかったら結果が返ってこずエラーになる。
    return this;
  }
});

var tasks = new Tasks([
  {
    title: 'title1',
    completed: true
  },
  {
    title: 'title2aaaaa'
  },
  {
    title: 'title3'
  }
]);
// console.log(tasks.toJSON());
var tasksView = new TasksView({collection: tasks});
$('#tasks').html(tasksView.render().el);
})();
